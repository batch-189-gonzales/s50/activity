import React from 'react';

//Create Context Object - stores information that can be shared across all components within the app
const UserContext = React.createContext()

// Provider component - allows other components to consume/use the context object and supply the necessary information needed to the context object
export const UserProvider = UserContext.Provider;

export default UserContext;