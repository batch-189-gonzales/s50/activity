//import {useState, useEffect} from 'react';
import {Row, Col, Card, Button} from 'react-bootstrap';
import {Link} from 'react-router-dom';

export default function CourseCard({courseProp}) {
	// console.log(props);
	//console.log(courseProp);

	/*
	Use the state hook for this component to be able to store its state.

	States are used to keep track of information related to individual components

	Syntax:
		const [getter, setter] = useState(initialGetterValue)
	*/

	// const [count, setCount] = useState(0);
	// const [seat, setSeat] = useState(30);

	// function enroll() {
	// 	if(seat>0) {
	// 		setCount(count+1);
	// 		setSeat(seat-1);
	// 	} /*else {
	// 		alert(`No more seats available for ${name}.`);
	// 	};*/
	// };

	// useEffect(() => {
	// 	if(seat===0) {
	// 		alert(`No more seats available.`);
	// 	};
	// }, [seat]);

	const {name, description, price, _id} = courseProp;
	console.log(courseProp);

	return(
		<Row className="mt-3 mb-3">
			<Col>
				<Card className="p-3">
					<Card.Body>
						<Card.Title>{name}</Card.Title>
						<Card.Subtitle className="mb-0">Description:</Card.Subtitle>
						<Card.Text className="mt-0">{description}</Card.Text>

						<Card.Subtitle className="mb-0">Price:</Card.Subtitle>
						<Card.Text className="mt-0">Php {price}</Card.Text>

{/*						<Card.Text className="mb-0">Enrollees: {count}</Card.Text>
						<Card.Text className="mt-0">Seats Available: {seat}</Card.Text>*/}

						<Button variant="primary" as={Link} to={`/courses/${_id}`}>Details</Button>
{/*						<Link className="btn btn-primary" to="/courseview">Details</Link>*/}
					</Card.Body>
				</Card>
			</Col>
		</Row>
	);
};